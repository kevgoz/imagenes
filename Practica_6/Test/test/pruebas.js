let chai = require("chai");


let chaiHttp = require("chai-http");
const expect = require("chai").expect;
const assert = require("chai").assert;
chai.use(chaiHttp);



const app = require("../app");
//const pagina = require("../../pagina/src/app/app.component.spec")
//const hello = app.hellowWord();
//const resta = app.substraction(5, 3);

describe("Test 1 para mostrar el nombre de la practica", () => {
  it("la prueba debe ser verdadera para el titulo igual a practica 4  ", () => {

    result = app.mostrar_titulo("practica 4")
    assert.equal(result,"practica 4")
    
  });
  
});


describe("Test 2 para mostrar el nombre del curso", () => {
    it("la prueba debe ser exitosa para el curso de software avanzado   ", () => {

      result = app.mostrar_curso("software avanzado")
      assert.equal(result,"software avanzado")
      

  });
});  

describe("Test 3 para mostrar el titulo de la practica", () => {
      it("la prueba debe ser exitosa para el el titulo sonarqube   ", () => {
    
        result = app.nombre_practica("sonarqube")
        assert.equal(result,"sonarqube")
        
    
  });  
});

describe("Test 4 para mostrar el nombre del grupo", () => {
  it("la prueba debe ser exitosa para el grupo 15  ", () => {

    result = app.no_grupo("sonarqube")
    assert.equal(result,"pareja 15")
    

});  
});

describe("Test 5 para mostrar el nombre del integrante", () => {
  it("la prueba debe ser exitosa para kelvin ", () => {

    result = app.integrante1("kelvin")
    assert.equal(result,"kelvin")
    

});  
});

describe("Test 4 para mostrar el nombre del integrante 2", () => {
  it("la prueba debe ser exitosa para Elder  ", () => {

    result = app.integrante2("Elder")
    assert.equal(result,"Elder")
    

});  
});