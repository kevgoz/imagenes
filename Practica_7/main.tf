terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.5.0"
    }
  }
}

provider "google" {
  credentials = file("./pagina/grupo15-343102-b804a110c12c.json")

  project = "grupo15-343102"
  region  = "us-central1"
  zone    = "us-central1-a"
}

resource "google_compute_firewall" "firewall_practica" {
  project = "grupo15-343102"
  name    = "f-externalssh"
  network = "default"
  allow {
    protocol = "tcp"
    ports    = ["8080", "80", "22"]
  }
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["externalssh"]
}

resource "google_compute_address" "static_add" {
  name       = "dir"
  depends_on = [google_compute_firewall.firewall_practica]
}



resource "google_compute_network" "vpc_network" {
  name = "terraform-network2"
}

resource "google_compute_instance" "vm_instance" {
  name         = "terraform-instance2"
  machine_type = "e2-medium"

  boot_disk {
    initialize_params {
      image = "ubuntu-1804-bionic-v20220213"
    }
  }

  #connection {
  #    type = "ssh"
  #    user = "kelvin_ingusac"
  #    host = google_compute_address.static_add.address
      
  #}
  #provisioner "remote-exec" {

   # inline = [
    #  "sudo apt-get update -y",
    #  "sudo apt-get upgrade -y",
    #  "sudo apt-get install docker.io -y",
    #  "sudo docker run -p 8088:80 -d vkelvin/pareja15",
    #]
  #}

  network_interface {
    network = google_compute_network.vpc_network.name

    access_config {
      nat_ip = google_compute_address.static_add.address
    }
  }

}
